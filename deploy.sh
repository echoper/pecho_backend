echo "###"
echo "Connecting to docker hub"
echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin
echo "###"

echo "###"
echo "Building pecho_backend"
docker build -t perecho/pecho_backend:latest  -f dependencies.Dockerfile .
echo "###"

echo "###"
echo "Pushing pecho_backend"
docker push perecho/pecho_backend:latest
echo "###"

echo "###"
echo "Building pecho_backend_celery"
docker build -t perecho/pecho_backend_celery:latest -f celery.Dockerfile .
echo "###"

echo "###"
echo "Pushing pecho_backend_celery"
docker push perecho/pecho_backend_celery:latest
echo "###"

echo "###"
echo "Calling webhook"
curl -H "Authorization: $WEBHOOK_TOKEN" $WEBHOOK_URL
echo "###"
