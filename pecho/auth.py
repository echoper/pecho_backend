from flask import jsonify, g
from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth

from . import db
from .models import User

basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth('Bearer')


@basic_auth.verify_password
def verify_password(username, password):
    if not username or not password:
        return False
    user = User.query.filter_by(username=username).first()
    if user is None or not user.verify_password(password):
        return False
    db.session.add(user)
    db.session.commit()
    g.current_user = user
    return True


@basic_auth.error_handler
def password_error():
    return (jsonify({'error': 'authentication required'}), 401,
            {'WWW-Authenticate': 'Basic realm="Authentication Required"'})


@token_auth.verify_token
def verify_token(token):
    user = User.query.filter_by(token=token).first()
    if user is None:
        return False
    db.session.add(user)
    db.session.commit()
    g.current_user = user
    return True


@token_auth.error_handler
def token_error():
    return (jsonify({'error': 'authentication required'}), 401,
            {'WWW-Authenticate': 'Bearer realm=@Authentication Required"'})
