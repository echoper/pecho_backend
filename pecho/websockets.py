from flask_socketio import emit, join_room

from . import db, socketio
from .models import ChatRoom


@socketio.on('connect')
def test_connect():
    emit('response', {'data': {'text': '''You are online. To begin: select
        (create) a room title and enter its password'''}})


@socketio.on('join')
def on_join(data):
    title = data['roomTitle']
    password = data['roomPassword']
    username = data['username']
    chat_room = ChatRoom.query.filter_by(title=title).first()
    if chat_room:
        if chat_room.password == password:
            join_room(chat_room.title)
    else:
        chat_room = ChatRoom(title=title, password=password)
        db.session.add(chat_room)
        db.session.commit()
        join_room(chat_room.title)
    emit('response', {'data': {'text': '%s is now connected to %s' % (
        username, title)}}, room=title)


@socketio.on('client message')
def test_client(message):
    from .tasks import save_message
    title = message['roomTitle']
    password = message['roomPassword']
    chat_room = ChatRoom.query.filter_by(
            title=title, password=password).first()
    if chat_room:
        save_message.apply_async(args=(message,))
        emit('response', {'data': message}, room=title)
    else:
        emit('response', {'data': {'text':
             'Select a room title and its password'}})


@socketio.on('start sound')
def start_sound(message):
    room = 'jamstart'
    join_room(room)
    emit('start sound', {'data': message}, room=room)


@socketio.on('stop sound')
def stop_sound(message):
    room = 'jamstop'
    join_room(room)
    emit('stop sound', {'data': message}, room=room)
