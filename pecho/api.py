from flask import Blueprint, request, abort, jsonify, g

from . import db
from .auth import basic_auth, token_auth
from .models import User, NWPost, ChatMessage, ChatRoom

api = Blueprint('api', __name__)


@api.route('/tokens', methods=['POST'])
@basic_auth.login_required
def get_token():
    if g.current_user.token is None:
        g.current_user.generate_token()
        db.session.add(g.current_user)
        db.session.commit()
    return jsonify({'token': g.current_user.token})


@api.route('/tokens', methods=['DELETE'])
@token_auth.login_required
def revoke_token():
    g.current_user.token = None
    db.session.add(g.current_user)
    db.session.commit()
    return '', 204


@api.route('/')
def hello_world():
    d = {}
    return jsonify(d)


@api.route('/users', methods=['POST'])
def new_user():
    user = User.create(request.get_json() or {})
    if User.query.filter_by(username=user.username).first() is not None:
        abort(400)
    db.session.add(user)
    db.session.commit()
    r = jsonify(user.to_dict())
    r.status_code = 201
    return r


@api.route('/NW_chat', methods=['GET', 'POST'])
@token_auth.login_required
def NoWebsocketPosts():
    if request.method == 'GET':
        posts = NWPost.query.all()
        return jsonify({'posts': [post.to_dict() for post in posts]})
    elif request.method == 'POST':
        post = NWPost.create(request.get_json() or {})
        db.session.add(post)
        db.session.commit()
        r = jsonify(post.to_dict())
        r.status_code = 201
    return jsonify({'test': 'Implementing non-websocket chat'})


@api.route('/NW_chat/<id>', methods=['PUT', 'DELETE'])
@token_auth.login_required
def NoWebsocketPost():
    if request.method == 'PUT':
        pass
    elif request.method == 'DELETE':
        pass
    return jsonify({'test': 'Implementing non-websocket chat'})


@api.route('/chat')
@token_auth.login_required
def chat_history():
    if request.args:
        title = request.args.get('title')
        password = request.args.get('password')
        if all([title, password]):
            chat_room = ChatRoom.query.filter_by(
                    title=title, password=password).first()
            messages = ChatMessage.query.filter_by(
                    chat_room_id=chat_room.id).all()
            return jsonify(
                    {'history': [message.to_dict() for message in messages]})


@api.route('/chat/rooms')
@token_auth.login_required
def chat_rooms():
    chat_rooms = ChatRoom.query.all()
    return jsonify({'chatrooms':  [room.to_dict() for room in chat_rooms]})
