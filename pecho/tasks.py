from . import db, celery
from .models import ChatMessage


@celery.task()
def save_message(message):
    chat_message = ChatMessage.create(message)
    db.session.add(chat_message)
    db.session.commit()
