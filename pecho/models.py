import binascii
import os
import datetime
from flask import abort, g
from werkzeug.security import generate_password_hash, check_password_hash

from . import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    username = db.Column(db.String(16), nullable=False, unique=True)
    password_hash = db.Column(db.String(256), nullable=False)
    token = db.Column(db.String(64), nullable=True, unique=True)
    nws_posts = db.relationship('NWPost', backref='user')
    chat_messages = db.relationship('ChatMessage', backref='user')

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_token(self):
        self.token = binascii.hexlify(os.urandom(32)).decode('utf-8')
        return self.token

    @staticmethod
    def create(data):
        user = User()
        user.from_dict(data, partial_update=False)
        return user

    def from_dict(self, data, partial_update=True):
        for field in ['username', 'password']:
            try:
                setattr(self, field, data[field])
            except KeyError:
                if not partial_update:
                    abort(400)

    def to_dict(self):
        return {
                'id': self.id,
                'created_at': self.created_at,
                'username': self.username
                }


class NWPost(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    text = db.Column(db.String(), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    @staticmethod
    def create(data, user=None):
        post = NWPost(user=user or g.current_user)
        post.from_dict(data, partial_update=False)
        return post

    def from_dict(self, data, partial_update=True):
        for field in ['text']:
            try:
                setattr(self, field, data[field])
            except KeyError:
                if not partial_update:
                    abort(400)

    def to_dict(self):
        return {
                'id': self.id,
                'created_at': self.created_at,
                'text': self.text,
                'username': self.user.username
                }


class ChatMessage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    text = db.Column(db.String(), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    chat_room_id = db.Column(db.Integer, db.ForeignKey('chat_room.id'))

    @staticmethod
    def create(data):
        user = User.query.filter_by(username=data['username']).first()
        chat_room = ChatRoom.query.filter_by(title=data['roomTitle']).first()
        message = ChatMessage(user=user, chat_room=chat_room)
        message.from_dict(data, partial_update=False)
        return message

    def from_dict(self, data, partial_update=True):
        for field in ['text']:
            try:
                setattr(self, field, data[field])
            except KeyError:
                if not partial_update:
                    abort(400)

    def to_dict(self):
        return {
                'id': self.id,
                'created_at': self.created_at,
                'text': self.text,
                'user_id': self.user_id,
                'username': self.user.username,
                'roomTitle': self.chat_room.title
                }


class ChatRoom(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(16), nullable=False, unique=True)
    password = db.Column(db.String(16), nullable=False)
    chat_messages = db.relationship('ChatMessage', backref='chat_room')

    def to_dict(self):
        return {
                'id': self.id,
                'title': self.title,
                }
