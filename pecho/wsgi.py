import os

from pecho import create_app

application = app = create_app(os.environ.get('FLACK_CONFIG', 'development'))
