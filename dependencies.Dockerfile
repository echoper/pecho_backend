FROM python:3.6-alpine3.7

RUN apk add  --update \
        alpine-sdk \
        python3 \
        python3-dev \
        build-base \
        linux-headers \
        pcre-dev \
        libressl-dev \
        postgresql-dev

WORKDIR /pecho_backend
COPY ./requirements.txt /pecho_backend

RUN pip3 install --trusted-host pypi.python.org -r requirements.txt

COPY . /pecho_backend


CMD [ "uwsgi", "--http", ":5000", \
               "--gevent", "100", \
               "--http-websockets", \
               "--master", \
               "--wsgi-file", "pecho/wsgi.py", \
               "--callable", "app" ]
