FROM perecho/pecho_backend

CMD [ "celery", "-A", "pecho.celery", "worker" ]
